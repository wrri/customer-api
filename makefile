include .env
export $(shell sed 's/=.*//' .env)

init:
	docker-compose build --force-rm --no-cache
	make up

up:
	docker-compose up -d
	make composer
	make schema-update
	make test
	echo "Application is running at http://localhost:9000 ..."

down:
	docker-compose down

restart: down up

composer:
	docker exec -it $(APP_NAME) php composer.phar install

schema-update:
	docker exec -it $(APP_NAME) php vendor/bin/doctrine orm:schema-tool:update --force

fixtures:
	docker exec -it $(APP_NAME) php bin/fixture-loader.php

test: fixtures
	docker exec -it $(APP_NAME) ./vendor/bin/phpunit src/Customer/Tests

cli:
	docker exec -it $(APP_NAME) bash
