<?php

use App\Kernel;

use Doctrine\Common\DataFixtures\Loader;
use App\Customer\Fixtures\CustomerFixtureLoader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use App\Core\Services\EntityManager;


require dirname(__DIR__).'/vendor/autoload.php';

$kernel = new Kernel('dev',  true);
$kernel->boot();
$container = $kernel->getContainer();

$entityManager = $container->get(EntityManager::class)->getEntityManager();

$loader = new Loader();
$loader->addFixture(new CustomerFixtureLoader());

$purger = new ORMPurger();
$executor = new ORMExecutor($entityManager, $purger);
$executor->execute($loader->getFixtures());
