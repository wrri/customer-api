<?php

namespace App\Core\Services;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager as ORMEntitymanager;

class EntityManager
{
    private $entitiesPath;
    private $isDebug;
    private $connectionParams;

    public function __construct(array $entitiesPath, bool $isDebug, array $connectionParams)
    {
        $this->entitiesPath = $entitiesPath;
        $this->isDebug = $isDebug;
        $this->connectionParams = $connectionParams;
    }

    public function getEntityManager(): EntityManagerInterface
    {
        $config = Setup::createAnnotationMetadataConfiguration($this->entitiesPath, $this->isDebug, null, null, false);
        $entityManager = ORMEntitymanager::create($this->connectionParams, $config);

        return $entityManager;
    }
}
