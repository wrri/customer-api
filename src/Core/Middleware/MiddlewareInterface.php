<?php

declare(strict_types=1);

namespace App\Core\Middleware;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface MiddlewareInterface
{
    public function __invoke(Request $request, RequestHandlerInterface $handler): Response;
}
