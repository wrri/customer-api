<?php

declare(strict_types=1);

namespace App;

use App\Core\Middleware\MiddlewareInterface;
use App\Core\Middleware\RequestHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class MiddlewarePipeline implements RequestHandlerInterface
{
    /**
     * @var MiddlewareInterface[]
     */
    private $queue;

    public function __construct(iterable $queue)
    {
        $this->queue = $queue;
    }

    public function handle(Request $request): Response
    {
        $middleware = array_shift($this->queue);
        if (null === $middleware) {
            throw new \LogicException('Middleware needs to return a response. Router middleware needs to be the last middleware');
        }

        return $middleware($request, $this);
    }
}
