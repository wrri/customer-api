<?php

declare(strict_types=1);

namespace App\Customer\Controller;

use App\Customer\Repository\GetCustomerCollectionRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class GetCustomerCollection
{
    private $getCustomerCollectionRepository;

    public function __construct(GetCustomerCollectionRepository $getCustomerCollectionRepository)
    {
        $this->getCustomerCollectionRepository = $getCustomerCollectionRepository;
    }

    public function __invoke(Request $request): Response
    {
        return new JsonResponse(($this->getCustomerCollectionRepository)());
    }
}
