<?php

declare(strict_types=1);

namespace App\Customer\Controller;

use App\Core\Services\EntityManager;
use App\Customer\Dto\CustomerRequestDto;
use App\Customer\Entity\Customer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CreateCustomer
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);
        $customerRequest = (new CustomerRequestDto())
            ->denormalize($data)
        ;

        $customer = Customer::createFromCustomerRequest($customerRequest);

        $em = $this->entityManager->getEntityManager();
        $em->persist($customer);
        $em->flush();

        return new JsonResponse($customer, Response::HTTP_CREATED);
    }
}
