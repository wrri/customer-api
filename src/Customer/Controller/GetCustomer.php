<?php

declare(strict_types=1);

namespace App\Customer\Controller;

use App\Core\Services\EntityManager;
use App\Customer\Entity\Customer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class GetCustomer
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(Request $request, string $id): Response
    {
        $em = $this->entityManager->getEntityManager();

        $customer = $em->getRepository(Customer::class)
            ->find($id)
        ;
        if (!$customer) {
            return new JsonResponse('Not Found', Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($customer, Response::HTTP_OK);
    }
}
