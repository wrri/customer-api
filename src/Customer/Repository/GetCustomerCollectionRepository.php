<?php

declare(strict_types=1);

namespace App\Customer\Repository;


use App\Core\Services\EntityManager;
use App\Customer\Entity\Customer;

final class GetCustomerCollectionRepository
{
    private const NB_ITEMS_PER_PAGE = 10;

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(): array
    {
        // will be limited to 10 items for demo purposes, should be retrunning a DoctrinePaginator
        return $this->entityManager->getEntityManager()
            ->getRepository(Customer::class)
            ->findBy([], ['firstName' => 'DESC'], self::NB_ITEMS_PER_PAGE)
        ;
    }

}
