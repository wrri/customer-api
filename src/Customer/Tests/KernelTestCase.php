<?php

declare(strict_types=1);

namespace App\Customer\Tests;

use App\Kernel;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\HttpClient;

class KernelTestCase extends TestCase
{
    protected static $container;

    public function boot(): void
    {
        $kernel = new Kernel('test',  true);
        $kernel->boot();
        self::$container = $kernel->getContainer();
    }

    public function createClient(string $method, string $path, array $option = [])
    {
        $client = HttpClient::create();
        return $client->request($method, sprintf('http://172.24.0.1:9000/%s', $path), $option);
    }
}
