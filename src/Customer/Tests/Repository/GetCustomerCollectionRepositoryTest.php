<?php

declare(strict_types=1);

namespace App\Customer\Tests\Repository;

use App\Customer\Entity\Customer;
use App\Customer\Repository\GetCustomerCollectionRepository;
use App\Customer\Tests\KernelTestCase;

class GetCustomerCollectionRepositoryTest extends KernelTestCase
{

    public function testGetCustomerCollection(): void
    {
        self::boot();
        $repository = self::$container->get(GetCustomerCollectionRepository::class);
        $customers = $repository();
        $this->assertCount(5, $customers);

        /** @var Customer $fooBar */
        $fooBar = $customers[3];
        $this->assertNotNull($fooBar);
        $this->assertSame('Foo', $fooBar->getFirstName());
        $this->assertSame('Foo', $fooBar->getLastName());
        $this->assertSame('Foo Foo', $fooBar->getName());
        $this->assertSame('Google', $fooBar->getCompanyName());
        $this->assertSame(null, $fooBar->getVatId());

        /** @var Customer $chuckNorris */
        $chuckNorris = $customers[4];
        $this->assertNotNull($chuckNorris);
        $this->assertSame('Chuck', $chuckNorris->getFirstName());
        $this->assertSame('Norris', $chuckNorris->getLastName());
        $this->assertSame('Chuck Norris', $chuckNorris->getName());
        $this->assertSame('Company 1', $chuckNorris->getCompanyName());
        $this->assertSame('1234', $chuckNorris->getVatId());
    }
}
