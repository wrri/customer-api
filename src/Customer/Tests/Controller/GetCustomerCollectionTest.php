<?php

declare(strict_types=1);

namespace App\Customer\Tests\Controller;

use App\Customer\Tests\KernelTestCase;

class GetCustomerCollectionTest extends KernelTestCase
{
    public function testGetCustomerCollection(): void
    {
        self::boot();

        $response = $this->createClient('GET', 'customers');

        $this->assertSame(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertCount(3, $data);
        $wissem = $data[0];
        $this->assertSame('Wissem Riahi', $wissem['name']);
        $this->assertSame('Company 1', $wissem['company']);
        $this->assertSame('vat1', $wissem['vat_id']);
    }

}
