<?php

declare(strict_types=1);

namespace App\Customer\Tests\Controller;

use App\Core\Services\EntityManager;
use App\Customer\Entity\Customer;
use App\Customer\Tests\KernelTestCase;

class GetCustomerTest extends KernelTestCase
{
    private $entityManager;

    public function setUp()
    {
        self::boot();
        $this->entityManager = self::$container->get(EntityManager::class)->getEntityManager();
    }

    public function testGetCustomerNotFound(): void
    {
        $response = $this->createClient('GET', 'customers/foobar');
        $this->assertSame(404, $response->getStatusCode());
    }

    public function testCustomerCompanyNameUpdateSuccessfully(): void
    {
        $chuckNorris = $this->entityManager->getRepository(Customer::class)
            ->findOneBy(['firstName' => 'Chuck'])
        ;

        $response = $this->createClient('GET', sprintf('customers/%s', $chuckNorris->getId()));
        $this->assertSame(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertNotNull($data['id']);
        $this->assertSame('Chuck Norris', $data['name']);
        $this->assertSame('Company 1', $data['company']);
        $this->assertSame('1234', $data['vat_id']);
    }
}
