<?php

declare(strict_types=1);

namespace App\Customer\Tests\Controller;

use App\Customer\Tests\KernelTestCase;

class CreateCustomerTest extends KernelTestCase
{
    public function setUp()
    {
        self::boot();
    }

    public function testCustomerCreation(): void
    {
        $response = $this->createClient('POST', 'customers', [
            'body' => json_encode([
                'firstName' => 'Wissem',
                'lastName' => 'Riahi',
                'companyName' => 'Company 1',
                'vatId' => 'vat1',
            ])
        ]);
        $this->assertSame(201, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertNotNull($data['id']);
        $this->assertSame('Wissem Riahi', $data['name']);
        $this->assertSame('Company 1', $data['company']);
        $this->assertSame('vat1', $data['vat_id']);
    }

    public function testInvalidCustomerWithoutFirstNameCreation(): void
    {
        $response = $this->createClient('POST', 'customers', [
            'body' => json_encode([
                'lastName' => 'last',
                'companyName' => 'Company 1',
                'vatId' => 'vat1',
            ])
        ]);
        $this->assertSame(500, $response->getStatusCode());
    }

    public function testInvalidCustomerWithoutLastNameCreation(): void
    {
        $response = $this->createClient('POST', 'customers', [
            'body' => json_encode([
                'firstName' => 'first',
                'companyName' => 'Company 1',
                'vatId' => 'vat1',
            ])
        ]);
        $this->assertSame(500, $response->getStatusCode());
    }

}
