<?php

declare(strict_types=1);

namespace App\Customer\Tests\Controller;

use App\Core\Services\EntityManager;
use App\Customer\Entity\Customer;
use App\Customer\Tests\KernelTestCase;

class UpdateCustomerCompanyNameTest extends KernelTestCase
{
    private $entityManager;

    public function setUp()
    {
        self::boot();
        $this->entityManager = self::$container->get(EntityManager::class)->getEntityManager();
    }

    public function testCustomerCompanyNameUpdateForInexistantCustomer(): void
    {
        $response = $this->createClient('PUT', 'customers/foobar', [
            'body' => json_encode([
                'foobar' => 'Amazon',
            ])
        ]);
        $this->assertSame(404, $response->getStatusCode());
    }

    public function testCustomerCompanyNameUpdateSuccessfully(): void
    {
        $customer = new Customer('Foo', 'Foo');
        $customer->setCompanyName('Google');
        $this->entityManager->persist($customer);
        $this->entityManager->flush();


        $response = $this->createClient('PUT', sprintf('customers/%s', $customer->getId()), [
            'body' => json_encode([
                'companyName' => 'Amazon',
            ])
        ]);
        $this->assertSame(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertNotNull($data['id']);
        $this->assertSame('Amazon', $data['company']);
    }

    public function testCustomerCompanyNameUpdateWithoutCompanyName(): void
    {
        $customer = new Customer('Foo', 'Foo');
        $customer->setCompanyName('Google');
        $this->entityManager->persist($customer);
        $this->entityManager->flush();


        $response = $this->createClient('PUT', sprintf('customers/%s', $customer->getId()), [
            'body' => json_encode([
                'foobar' => 'Amazon',
            ])
        ]);
        $this->assertSame(400, $response->getStatusCode());
    }

}
