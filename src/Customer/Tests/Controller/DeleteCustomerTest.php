<?php

declare(strict_types=1);

namespace App\Customer\Tests\Controller;

use App\Core\Services\EntityManager;
use App\Customer\Entity\Customer;
use App\Customer\Tests\KernelTestCase;

class DeleteCustomerTest extends KernelTestCase
{
    private $entityManager;

    public function setUp()
    {
        self::boot();
        $this->entityManager = self::$container->get(EntityManager::class)->getEntityManager();
    }

    public function testCustomerDeleteForInexistantCustomer(): void
    {
        $response = $this->createClient('DELETE', 'customers/foobar');
        $this->assertSame(404, $response->getStatusCode());
    }

    public function testCustomerDeleteSuccessfully(): void
    {
        $customer = new Customer('Joe12', 'Doe');
        $customer->setCompanyName('Google');
        $this->entityManager->persist($customer);
        $this->entityManager->flush();


        $response = $this->createClient('DELETE', sprintf('customers/%s', $customer->getId()));
        $this->assertSame(202, $response->getStatusCode());

        $this->entityManager->clear(Customer::class);

        $customerExists = $this->entityManager->getRepository(Customer::class)
            ->findOneBy(['firstName' => 'Joe123'])
        ;

        $this->assertNull($customerExists);
    }

}
