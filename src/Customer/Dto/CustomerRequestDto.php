<?php

declare(strict_types=1);

namespace App\Customer\Dto;

class CustomerRequestDto
{
    /**
     * @Assert\NotBlank
     */
    public $firstName;

    /**
     * @Assert\NotBlank
     */
    public $lastName;

    public $companyName;

    public $vatId;

    public function denormalize(array $data): self
    {
        $class = new self();
        foreach ($data as $key => $value) {
            $class->{$key} = $value;
        }

        return $class;
    }
}
