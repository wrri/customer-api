<?php

declare(strict_types=1);

namespace App\Customer\Fixtures;


use App\Customer\Entity\Customer;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class CustomerFixtureLoader implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $customer = $this->createCustomer('Chuck', 'Norris', 'Company 1', '1234');

        $manager->persist($customer);

        $customer = $this->createCustomer('Foo', 'Bar', 'Company 1', '1234');
        $manager->persist($customer);

        $manager->flush();
    }

    private function createCustomer(string $firstName, string $lastName, string $company, string $vatId): Customer
    {
        $customer = new Customer($firstName, $lastName);

        $customer->setCompanyName($company)
            ->setVatId($vatId)
        ;

        return $customer;
    }
}
