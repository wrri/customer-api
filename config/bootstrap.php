<?php

require dirname(__DIR__).'/vendor/autoload.php';

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Yaml\Yaml;

$_SERVER['APP_ENV'] = $_ENV['APP_ENV'] = ($_SERVER['APP_ENV'] ?? $_ENV['APP_ENV'] ?? null) ?: 'dev';
$_SERVER['APP_DEBUG'] = $_SERVER['APP_DEBUG'] ?? $_ENV['APP_DEBUG'] ?? 'prod' !== $_SERVER['APP_ENV'];

$kernel = new \App\Kernel($_SERVER['APP_ENV'],  $_SERVER['APP_DEBUG']);
$kernel->boot();
$container = $kernel->getContainer();

$routes = new RouteCollection();

$routesConfig = $value = Yaml::parseFile(__DIR__.'/routes.yaml');
foreach ($routesConfig as $routeId => $routeParams) {
    $route = new Route($routeParams['path'], [
            '_controller' => $container->get($routeParams['controller']),
        ]
    );
    $route->setMethods($routeParams['methods']);
    $routes->add($routeId, $route);
}

$request = Request::createFromGlobals();

$matcher = new UrlMatcher($routes, new RequestContext());

$dispatcher = new EventDispatcher();
$dispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));
