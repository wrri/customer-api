<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use App\Kernel;
use App\Core\Services\EntityManager;

require dirname(__DIR__).'/vendor/autoload.php';

$kernel = new Kernel('dev',  true);
$kernel->boot();
$container = $kernel->getContainer();

$entityManager = $container->get(EntityManager::class)->getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);
