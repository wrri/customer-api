# Customer API

Basic micro service bolier plate for an API example based on HttpKernel and Doctrine.

## Installation

Clone this repo and run:

```bash
$ make init
```

It will download the containers, start the app and run the test suite.

To run the tests seperately:
```bash
$ make test
```
